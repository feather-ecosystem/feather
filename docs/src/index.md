# Feather.jl

Feather.jl is a set of finite element analysis tool focusing on enabling multi-physics simulations using modern technologies build around isogeometric analysis, compatible discretizations, as well as fast formation and assembly techniques leveraging tensor-product spaces and sum factorization.

## Core ideas

Software development within a group of scientific researchers (i.e. postdocs and Ph.D. students) will inevitably lead to a fragmented code base. This is by the nature of the scientific work itself, where we usually pursue different goals on the path to the degree. Some of the code sections will become highly specialized and thus in the worst case only maintainable by the responsible researcher himself. Considering how dynamic the research groups are, some researchers will move onto other projects. Because of that we introduce certain work and design principles around the Feather project.

> The first 90 percent of the code accounts for the first 90 percent of the development time. The remaining 10 percent of the code accounts for the other 90 percent of the development time. *Tom Cargill*

## Feather ecosystem

The core design choice is to introduce small packages, which, in the spirit of Unix's DOTADIW *(Do one thing and do it well)*, stay coherent in the functionality they implement. These packages can be compared to basic C libraries like `cstdlib`, `cstdio`, `cmath` just to name a few. The core packages are meant to interface with each other and create an [`ecosystem`](https://gitlab.com/feather-ecosystem/) which can be used to implement complex multi-physics models and numerical methods.

The key Finite Element Analysis interfacing mechanisms are defined in [`FeaInterfaces`](https://gitlab.com/feather-ecosystem/FeaInterfaces). Defining a clear interface provides the maintainers with an overview of the relationships between certain submodules. Furthermore, if some submodule breaks the code base due to a recent contribution, the maintainers can isolate the issue more easily. Submodules which have been maintained by someone for a period and became abandoned can be identified easily, too. Replacing an outdated submodule by a new and better performing implementation is less painful as well. Since other packages can relay on the implemented interface. That is why it is of outer importance to carefully design the interface for all the packages.

| Core package           | Short description                                                                                 |
| :--------------------- | :------------------------------------------------------------------------------------------------ |
| [`FeaInterfaces`](https://gitlab.com/feather-ecosystem/FeaInterfaces)     | interfaces between Feather packages                                                               |
| [`SortedSequences`](https://gitlab.com/feather-ecosystem/SortedSequences)       | basic data structures with specific properties, like monotonic increasing values                  |
| KroneckerProducts      | structures for kronecker operators, basis functions, collocation matrices, etc.                   |
| CartesianProducts      | structures for cartesian indices, iterators, etc.                                                 |
| [`UnivariateSplines`](https://gitlab.com/feather-ecosystem/UnivariateSplines)    | structures and evaluation routines for univariate splines spaces, as well as refinement operators |
| TensorProductBsplines  | B-spline mappings                                                                                 |
| NURBS                  | NURBS mappings                                                                                    |
| BezierBernsteinMethods | splines on simplicial meshes                                                                      |

## Private registry

For development purposes we maintain a private Julia [`repository`](https://gitlab.com/feather-ecosystem/FeatherRegistry), which provides all the packages in the Feather ecosystem. Users can add it to their Julia setup as follows

```julia-repl
pkg> registry add git@gitlab.com:feather-ecosystem/FeatherRegistry.git
```

With that we can handle the Feather ecosystem packages like any other Julia package

```julia-repl
pkg> add UnivariateSplines
pkg> dev UnivariateSplines
pkg> test UnivariateSplines
pkg> add UnivariateSplines@0.7.2
pkg> add UnivariateSplines#master
```

!!! note
    This documentation references the latest stable versions of the core packages. The goal of all packages is to support latest versions of other packages. Sometimes the transition takes time. Some due to compatibility constraints packages might still use an older version of a package in the Feather ecosystem. In that case reference the respective package documentation of that package version in its Gitlab repo.


