# Frequently asked questions

## Why are my SSH keys not working?
If Julia keeps asking for the ssh key, it means that the `ssh-agent` is probably not running properly and/or the keys have not been added to the ssh-agent

```bash
$ ssh-add /path/to/key
```

You can check which keys are loaded using

```bash
$ ssh-add -L
```

For possible solutions see [here](https://stackoverflow.com/questions/18880024/start-ssh-agent-on-login) and [here](https://discourse.julialang.org/t/pkg-clone-keeps-asking-for-my-ssh-key/15155/2).

## Why is Julia failing to add Feather packages?
If you just installed Julia you might be missing the general Julia registry (for some reason it is not included in some Linux distributions, e.g. Ubuntu 20.04 LTS).
```julia
pkg> registry add git@gitlab.com:feather-ecosystem/FeatherRegistry.git
    Cloning registry from "git@gitlab.com:feather-ecosystem/FeatherRegistry.git"
      Added registry `FeatherRegistry` to `~/.julia/registries/FeatherRegistry`
pkg> add UnivariateSplines
   Updating registry at `~/.julia/registries/FeatherRegistry`
   Updating git-repo `git@gitlab.com:feather-ecosystem/FeatherRegistry.git`
  Resolving package versions...
ERROR: cannot find name corresponding to UUID 23992714-dd62-5051-b70f-ba57cb901cac in a registry
```

To fix this just run

```julia-repl
pkg> registry add https://github.com/JuliaRegistries/General
```

## How do I contribute?
You can develop a package using
```julia-repl
pkg> dev UnivariateSplines
    Cloning git-repo `git@gitlab.com:feather-ecosystem/UnivariateSplines.git`
  Resolving package versions...
Updating `~/.julia/environments/v1.5/Project.toml`
  [2552f6ce] + UnivariateSplines v0.7.2 `~/.julia/dev/UnivariateSplines`
Updating `~/.julia/environments/v1.5/Manifest.toml`
  [2552f6ce] ~ UnivariateSplines v0.7.2 ⇒ v0.7.2 `~/.julia/dev/UnivariateSplines`
```
The package in development `UnivariateSplines` is now pointing to `~/.julia/dev/UnivariateSplines`.
You can work on it there and then push this git repository to you private remote repository on GitLab.
Then just open a pull request. For more details see the [Development](@ref) section.