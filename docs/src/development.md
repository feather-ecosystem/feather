# Development

## FeatherRegistry.jl

This registry allows you to use private packages from Feather. It 
is created using the [LocalRegistry.jl](https://github.com/GunnarFarneback/LocalRegistry.jl) 
package.

### Usage

If you're using at least Julia 1.1, then you can add this registry in the Pkg REPL
using
```julia-repl
pkg> registry add git@gitlab.com:feather-ecosystem/FeatherRegistry.git
```
You do need to be able to verify your identity using ssh. Once setup, you can use private
packages as if they are standard public packages from the general Julia package registry. 

As described in [Pkg.jl](https://julialang.github.io/Pkg.jl/v1/managing-packages/) 
packages can be added in one of two ways. If you simply want to use the functionality 
from a package the you can `add` a package
```julia-repl
pkg> add PkgRelease
```
If you want to extend upon existing functionality then choose `develop`
```julia-repl
pkg> develop SortedSequences
```

### Registering your package with FeatherRegistry
You can register your package using the `PkgRelease` package, as shown in the following section.

## PkgRelease.jl

The process of registering (a new version of) your package to the private `FeatherRegistry` is automated by our `PkgRelease.jl`.
If your ssh-keys have been properly set and you've added the `FeatherRegistry` to your environment, then `PkgRelease.jl` is available to you as
```julia-repl
julia> Pkg.add("PkgRelease");
julia> using PkgRelease
```

Releasing the first version or a new version of your package is simple. First activate the project you wish to release
```julia-repl
julia> Pkg.activate("MyPackage")
```
Subsequently, following the rules of [SemVer](https://semver.org/), you can release you package.
#### `Patch` release
```julia-repl
julia> release(PkgRelease.Patch)
```
#### `Minor` release
```julia-repl
julia> release(PkgRelease.Minor)
```
#### `Major` release
```julia-repl
julia> release(PkgRelease.Major)
```

The following steps are performed automatically by `PkgRelease.jl`:
1. Incrementing the version number in your `Project.toml`
2. Registering your package to the registry using [LocalRegistry.jl](https://github.com/GunnarFarneback/LocalRegistry.jl)
3. Generating Git tags of the package and the private registry and pushing all changes to the remote repository.

## FeatherPkgTemplates.jl

## Code style

We endorse the standard coding guide lines provided in the Julia [documentation](https://docs.julialang.org/en/v1/manual/style-guide/).
Besides that all core packages in the `ecosystem` are meant to DOTADIW (_Do one thing and do it well_), so stay coherent in the functionality
your packages implement.