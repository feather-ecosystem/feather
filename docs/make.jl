# to be build using 
# julia --project=docs -e 'using Pkg; Pkg.develop(PackageSpec(path=pwd())); Pkg.instantiate(); '; julia --project=docs --color=yes docs/make.jl

using Documenter, Pkg, LibGit2, TOML
include("../src/corepackages.jl")

manifest = TOML.parsefile("Manifest.toml")

##region (get packages as defined in Feather.core_packages)
rm("docs/src/external", force=true, recursive=true); mkdir("docs/src/external")
rm("core", force=true, recursive=true); mkdir("core")

for pkg in core_packages
    repo = LibGit2.clone(pkg.repo.source, joinpath("core", pkg.name))
    githash = manifest[pkg.name][1]["git-tree-sha1"] 
    tree = LibGit2.GitObject(repo, githash)
    LibGit2.checkout_tree(repo, tree)
    Pkg.add(PackageSpec(path=joinpath(pwd(), "core", pkg.name)))
end

packages_info = Dict()
for pkg in readdir("core")
    source = joinpath(pwd(), "core", pkg, "docs", "src")
    target = joinpath(pwd(), "docs", "src", "external", pkg)
    cp(source, target)
    
    source = joinpath(pwd(), "core", pkg, "docs", "src", "assets", pkg)
    target = joinpath(pwd(), "docs", "src", "assets", pkg)
    cp(source, target; force=true)

    include(joinpath(pwd(), "core", pkg, "docs", "config.jl")) # def package_info
    push!(packages_info, pkg => package_info)
end
##endregion

##region (build page tree and collect docs modules)
modules = Array{Module, 1}()
pages = []

push!(pages, "Home" => "index.md")
for (pkg, info) in packages_info
    append!(modules, collect(info["modules"]))
    push!(pages, pkg => [])
    curr_pages_dict = pages[end][2]

    # todo: pages might break if the structure is different [check that!]
    for (title, src) in info["pages"]
        push!(curr_pages_dict, title => joinpath("external", pkg, src))
    end
end
push!(pages, "Development" => "development.md")
push!(pages, "FAQ" => "faq.md")

modules = unique(modules)
##endregion

##region (build docs)
makedocs(
    modules  = modules,
    sitename = "Feather.jl",
    authors  = "Michał Mika <michal@mika.sh> and contributors",
    pages = pages,
    repo = "https://gitlab.com/feather-ecosystem/Feather/blob/{commit}{path}#{line}",
    format = Documenter.HTML(
        collapselevel = 1,
    ),
)
##endregion

##region (a hacky fix of edit and source links for subdocs)
function substitute_page_links(fname::String, pkgname::String, branch::String)
    content = ""
    open(fname, "r") do io
        content = read(io, String)
    end
    open(fname, "w") do io
        content = replace(content, r"(<a class=\"docs-sourcelink\".*?)(Feather)(.*?\")"m => SubstitutionString("\\g<1>$pkgname\\g<3>"))
        content = replace(content, r"(<a class=\"docs-edit-link\".*?)(Feather)(\/blob\/)(.*?)(\/.*?)external.*?\/.*?\/(.*?\")" => SubstitutionString("\\g<1>$pkgname\\g<3>$branch\\g<5>\\g<6>"))
        write(io, content)
    end
end

branch = Dict()
map(pkg -> push!(branch, pkg.name => pkg.repo.rev), core_packages)

for (root, dirs, files) in walkdir("docs/build/external/")
    for file in files
        if file[end-3:end] == "html"
            pkgname = splitpath(root)[4]
            htmlfile = joinpath(root, file)
            substitute_page_links(htmlfile, pkgname, branch[pkgname])
        end
    end
end
##endregion