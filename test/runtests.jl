using Test, Pkg, Feather

@testset "Feather" begin
    @test isdefined(Feather, :core_packages) == true
    @test length(core_packages) > 0
end # @testset

for pkg in core_packages
    module_name = Symbol(pkg.name)
    @eval using $module_name
end

for pkg in core_packages
    Pkg.test(pkg)
end

