# Feather

[![pipeline status](https://gitlab.com/feather-ecosystem/Feather/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/Feather/-/commits/master)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/feather/)

This package collects the documentation of the latest Feather ecosystem packages in one place.