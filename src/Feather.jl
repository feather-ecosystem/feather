module Feather

export core_packages

include("corepackages.jl")

for pkg in core_packages
    module_name = Symbol(pkg.name)
    @eval import $module_name
end
include("shorthands.jl")

# alternative way without prefixes
using Reexport
for pkg in core_packages
    module_name = Symbol(pkg.name)
    @eval @reexport using $module_name
end

end # module
