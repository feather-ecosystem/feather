const unispl = UnivariateSplines
const srtseq = SortedSequences
const cprods = CartesianProducts
const tpbspl = TensorProductBsplines
const nurbs = NURBS
#const feaint = FeaInterfaces
#const kprods = KroneckerProducts

export unispl, srtseq, cprods, tpbspl, nurbs