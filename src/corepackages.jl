using Pkg: PackageSpec

core_packages = [
    PackageSpec(
        name="SortedSequences",
        url="git@gitlab.com:feather-ecosystem/SortedSequences.git",
        rev="master"),
    PackageSpec(
        name="UnivariateSplines",
        url="git@gitlab.com:feather-ecosystem/UnivariateSplines.git",
        rev="master"),
    PackageSpec(
        name="CartesianProducts",
        url="git@gitlab.com:feather-ecosystem/CartesianProducts.git",
        rev="master"),
    PackageSpec(
        name="BezierBernsteinMethods",
        url="git@gitlab.com:feather-ecosystem/core/BezierBernsteinMethods.git",
        rev="master"),
    PackageSpec(
        name="TensorProductBsplines",
        url="git@gitlab.com:feather-ecosystem/TensorProductBsplines.git",
        rev="master"),
    PackageSpec(
        name="NURBS",
        url="git@gitlab.com:feather-ecosystem/NURBS.git",
        rev="master"),
    PackageSpec(
        name="FeaInterfaces",
        url="git@gitlab.com:feather-ecosystem/FeaInterfaces.git",
        rev="master"),
    PackageSpec(
        name="KroneckerProducts",
        url="git@gitlab.com:feather-ecosystem/KroneckerProducts.git",
        rev="master"),
]